import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { retry, map, filter  } from "rxjs/operators";
import { Subscriber, Subscription } from 'rxjs';
import { log } from 'util';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ]
})
export class RxjsComponent implements OnInit, OnDestroy {

  subscription:Subscription
  constructor() {
   
   this.subscription= this.regresaObservable()
    //.pipe(retry(3))
      .subscribe(numero=>{
      //cuando se recibe un next
      console.log('Subs ', numero);
    },
    error=>console.error('error'),
    ()=>console.log('el observador terminó')
    )
   }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    console.log('la pagina se va a cerrar');
    this.subscription.unsubscribe()

  }

  regresaObservable():Observable<any>{
    
    return new Observable((observer:Subscriber<any>
    )=>{
      let contador =0
        let intervalo = setInterval(() => {
          contador ++

          const salida ={
            valor:contador
          }


          observer.next(salida)
        /*  if(contador===3){
            clearInterval(intervalo)
            observer.complete()
          }
          if(contador===2){
            clearInterval(intervalo)
            observer.error('el errorsazo!')
          }*/
            
        }, 1000);
    }).pipe(
      map(resp=>{  return resp.valor}),
      filter((value,index)=>{
       return ((value % 2)===1)

      })
    )
  }

}
