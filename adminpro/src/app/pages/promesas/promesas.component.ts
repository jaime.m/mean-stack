import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: [
  ]
})
export class PromesasComponent implements OnInit {

  constructor() { 
    let contador=0
    this.contarTest().then(()=>{
    console.log('terminó')
  }).catch(
    error=>console.error('Error en la promesa', error)
  )

  }

  contarTest():Promise<boolean>{
    return new Promise((res,rej)=>{
      let contador=0
      let intervalo=  setInterval(() => {
        contador++
          console.log(contador);
          if(contador===3){
          clearInterval(intervalo)
            res(true)
          }
        }, 1000)
      })
  }
  ngOnInit(): void {
  }

}
