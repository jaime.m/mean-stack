import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
ajustes:Ajustes={
  tema:'default',
  temaUrl:'assets/css/colors/default.css'
}
  constructor(@Inject(DOCUMENT) private _document) { 
    this.cargarAjustes()
  }

  guardarAjusted(){
    console.log('Guardado en el local storage');
    
    localStorage.setItem('ajustes',JSON.stringify(this.ajustes))
  }

  cargarAjustes(){
    if(localStorage.getItem('ajustes')){
      this.ajustes= JSON.parse(localStorage.getItem('ajustes'))
      console.log('Cargando del local storage');
      this.aplicarTema(this.ajustes.tema)
      
    }else{
      console.log('Usando valores por defecto');
      
    }
  }
  aplicarTema(tema:string){
    
    let url =`assets/css/colors/${tema}.css`
    this._document.getElementById('tema').setAttribute('href',url)

    this.ajustes.tema=tema
    this.ajustes.temaUrl=url
    this.guardarAjusted()
  }
}

interface Ajustes{
temaUrl:string,
tema:string
}