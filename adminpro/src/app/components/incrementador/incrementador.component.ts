import { Component, OnInit, Input, Output,EventEmitter, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: [
  ]
})
export class IncrementadorComponent implements OnInit {

  @ViewChild('txtProgress') txtProgress: ElementRef
  constructor() { }
  
  @Input() leyenda:string ='Leyenda'// valor por defecto

  @Input() progreso:number=50

  @Output() cambioValor:EventEmitter<number> = new EventEmitter()

  ngOnInit(): void {
  }

  onChange(newValue:number){

   // let elementHtml:any = document.getElementsByName('progreso')[0]

    if(newValue>=100)
      newValue=100
    else if(newValue<=0)
      newValue=0

    //elementHtml.value=this.progreso
    this.txtProgress.nativeElement.value=this.progreso
    this.txtProgress.nativeElement.focus()
    this.cambioValor.emit(newValue)
  }

  cambiarValor(valor:number){
    if(this.progreso==100  ){
      this.progreso=100
      return
    }
    if(this.progreso==0){
      this.progreso=0
      return
    }

    this.progreso+=valor
    this.cambioValor.emit(this.progreso)
  }



}
