const mongoose = require('mongoose')
const {Schema} =mongoose
const uniqueValidator= require('mongoose-unique-validator')

const rolesValidos={
    values:['ADMIN_ROLE','USER_ROLE'],
    message:"{VALUE} no es un rol permitido"
}

const userSchema = new Schema ({
    nombre:{type:String, required:[true,'El nombre es requerido']},
    email:{type:String, unique:true,required:[true,'El correo es requerido']},
    password:{type:String, required:[true,'la contraseña es requerida']},
    img:{type:String, required:false},
    role:{type:String, default:'USER_ROLE',enum:rolesValidos}
})

userSchema.plugin(uniqueValidator,{message:'El {PATH} debe ser único '})


module.exports= mongoose.model('user',userSchema)