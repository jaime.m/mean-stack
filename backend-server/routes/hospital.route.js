const express = require('express')
const app = express()
const bcrypt = require('bcryptjs')
const hospitales = require('../models/hospital.model')
const jsonwebtoken = require('jsonwebtoken')
const {SEED} = require('../config/config')
const middlewareAuth = require('../middlewares/auth.middleware')

app.get('/',(req,res)=>{

    let desde =req.query.desde || 0
    desde =Number(desde)

let user =  hospitales.find({},'nombre img usuario')
.populate('usuario', 'nombre email')
.skip(desde)
.limit(5)
.exec(
    (err,hospitals)=>{
    if(err){
        return res.status(400).json({
            message:"Error Cargando hospital",
            errors:err,
            ok:false
        })
    }
    hospitales.count((err,cantidad)=>{
        res.status(200).json({
            hospitals,
            ok:true,
            total:cantidad
        })
    })
    })
})


app.post('/', async (req,res)=>{
try{

    body = req.body

    let hosp = new hospitales({
        nombre:body.nombre,
        usuario:body.usuario

    })
   const hospitalesave= await hosp.save()
   if(hospitalesave){
     res.status(200).json({
        hospitalesave,
        ok:true
    })
   }
}catch(err){
    return res.status(500).json({
        mensaje:"error al crear hospital",
        ok:false,
        errors:err
    })
}
})

app.patch('/:id', async (req,res)=>{
    const id= req.params.id
    try{
        body = req.body

      let hosp= await  hospitales.findByIdAndUpdate(id,body,{new:true})
       if(hosp){
         res.status(200).json({
            hosp,
            ok:true
        })
       }
    }catch(err){
        return res.status(500).json({
            mensaje:"error al actualizar el hospital",
            ok:false,
            errors:err
        })
    }
    })

    
app.delete('/:id', async (req,res)=>{
    const id= req.params.id

    try{
        body = req.body

      let hosp= await  hospitales.findByIdAndDelete(id)
       if(hosp){
         res.status(200).json({
            hosp,
            ok:true
        })
       }
    }catch(err){
        return res.status(500).json({
            mensaje:"Error al eliminar el hospital",
            ok:false,
            errors:err
        })
    }
    })
    

module.exports=app