const express = require('express')
const app = express()
const bcrypt = require('bcryptjs')
const jsonwebtoken = require('jsonwebtoken')
const {SEED} = require('../config/config')
const usuarios = require('../models/user.model')

app.post('/',async (req,res)=>{


    try{    
    let user = await    usuarios.findOne({
        email:req.body.email
        })

        console.log(user);
        
        if(user){
            if(!bcrypt.compareSync(req.body.password,user.password)){
                return res.status(500).json({
                    ok:false,
                    mensaje:'Error de autenticación'
                })
            }
            //Crear el token!
            let token=jsonwebtoken.sign({usuario:user},SEED,{expiresIn:14400})

            res.status(200).json({
                ok:true,
                usuario:user,
                id:user._id,
                token
            })
        }else{
            return res.status(500).json({
                ok:false,
                mensaje:'Error de autenticación 1'
            })
        }

    }catch(err){
        return res.status(500).json({
            ok:false,
            mensaje:err
        })

    }


})

module.exports=app