const express = require('express')
const app = express()
const bcrypt = require('bcryptjs')
const medicos = require('../models/doctors.model')
const jsonwebtoken = require('jsonwebtoken')
const {SEED} = require('../config/config')
const middlewareAuth = require('../middlewares/auth.middleware')

app.get('/',(req,res)=>{

    let desde =req.query.desde || 0
    desde =Number(desde)
let user =  medicos.find({},'nombre img usuario hospital')
.populate('usuario', 'nombre email')
.populate('hospital')
.skip(desde)
.limit(5)
.exec(
    (err,medico)=>{
    if(err){
        return res.status(400).json({
            message:"Error Cargando medico ",
            errors:err,
            ok:false
        })
    }
    medicos.count((err,cantidad)=>{
        res.status(200).json({
            medico,
            ok:true,
            total:cantidad
        })
    })
    })
})


app.post('/', async (req,res)=>{
try{

    body = req.body

    let doc = new medicos({
        nombre:body.nombre,
        usuario:body.usuario,
        hospital:body.hospital

    })
   const medicosave= await doc.save()
   if(medicosave){
     res.status(200).json({
        medicosave,
        ok:true
    })
   }
}catch(err){
    return res.status(500).json({
        mensaje:"error al crear medico",
        ok:false,
        errors:err
    })
}
})

app.patch('/:id', async (req,res)=>{
    const id= req.params.id
    try{
        body = req.body

      let doc= await  medicos.findByIdAndUpdate(id,body,{new:true})
       if(doc){
         res.status(200).json({
            doc,
            ok:true
        })
       }
    }catch(err){
        return res.status(500).json({
            mensaje:"error al actualizar el medico",
            ok:false,
            errors:err
        })
    }
    })

    
app.delete('/:id', async (req,res)=>{
    const id= req.params.id

    try{
        body = req.body

      let doc= await  medicos.findByIdAndDelete(id)
       if(doc){
         res.status(200).json({
            doc,
            ok:true
        })
       }
    }catch(err){
        return res.status(500).json({
            mensaje:"Error al eliminar el medico",
            ok:false,
            errors:err
        })
    }
    })
    

module.exports=app