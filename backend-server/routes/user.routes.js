const express = require('express')
const app = express()
const bcrypt = require('bcryptjs')
const usuarios = require('../models/user.model')
const jsonwebtoken = require('jsonwebtoken')
const {SEED} = require('../config/config')
const middlewareAuth = require('../middlewares/auth.middleware')

app.get('/',async (req,res)=>{

    let desde =req.query.desde || 0
    desde =Number(desde)

let user =   usuarios.find({},'nombre img role email')
.skip(desde)
.limit(5)
.exec(
    (err,users)=>{
    if(err){
        return res.status(400).json({
            message:"Error Cargando usuario",
            errors:err,
            ok:false
        })
    }

    usuarios.count((err,cantidad)=>{
        res.status(200).json({
            users,
            ok:true,
            total:cantidad
        })
    })

    })
})


app.post('/', async (req,res)=>{
try{

    body = req.body
    let user = new usuarios({
        nombre:body.nombre,
        email:body.email,
        password: bcrypt.hashSync( body.password,10),
        role:body.role

    })

   const usuarioSave= await user.save()
   if(usuarioSave){
     res.status(200).json({
        usuarioSave,
        ok:true
    })
   }
}catch(err){
    return res.status(500).json({
        mensaje:"error al crear usuario",
        ok:false,
        errors:err
    })
}
})

app.patch('/:id', async (req,res)=>{
    const id= req.params.id
    try{
        body = req.body

      let user= await  usuarios.findByIdAndUpdate(id,body,{new:true})
       if(user){
         res.status(200).json({
            user,
            ok:true
        })
       }
    }catch(err){
        return res.status(500).json({
            mensaje:"error al actualizar el usuario",
            ok:false,
            errors:err
        })
    }
    })

    
app.delete('/:id', async (req,res)=>{
    const id= req.params.id

    try{
        body = req.body

      let user= await  usuarios.findByIdAndDelete(id)
       if(user){
         res.status(200).json({
            user,
            ok:true
        })
       }
    }catch(err){
        return res.status(500).json({
            mensaje:"Error al eliminar el usuario",
            ok:false,
            errors:err
        })
    }
    })
    

module.exports=app