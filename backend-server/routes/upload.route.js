const express = require('express')
const app = express()
const usuerio=require('../models/user.model')
const medico=require('../models/doctors.model')
const hospital=require('../models/hospital.model')
const fileUpload = require('express-fileupload')
var fs = require('fs')
app.use(fileUpload())


app.put('/:tipo/:id',(req,res)=>{

    let tipo = req.params.tipo
    let id = req.params.id
    if(!req.files){
        res.status(400).json({
            message:"No se seleccionó nada",
            ok:false
        })
    }
    //tipos colección
    let tiposValidos=['hospitales','medicos','usuarios']
    if(tiposValidos.indexOf(tipo)===-1){
        res.status(400).json({
            message:"Tipo no válido",
            ok:false,
            error:{message:`los tipos validos son ${tiposValidos.join(',')}`}
        })
    }
    let nombreArchivo= req.files.imagen
    let vector =nombreArchivo.name.split('.')
    let extension =vector[vector.length-1]

    let extensionesAceptadas =['png','jpg','gif', 'jpeg']

    if(extensionesAceptadas.indexOf(extension)===-1){
        res.status(400).json({
            message:"Extensión no valida",
            ok:false,
            error:{message:`las extensiones válidas son ${extensionesAceptadas.join(',')}`}
        })
    }

    //Crear un nombre de archivo personalizado

    let name =`${id}-${new Date().getMilliseconds()}.${extension}`

    //mover el archivo de un temporal al path

    var path= `${__dirname}\\uploads\\${tipo}\\${name}`
    console.log(path)
    nombreArchivo.mv(path,err=>{
        if(err){
           return res.status(400).json({
                message:"Error al mover archivo",
                ok:false,
                error:err
            }) 
        }
        subirPorTipo(tipo,id,name, res)

        /*res.status(200).json({
            message:"Archivo movido",
            ok:true
        })*/

    })
})

async function subirPorTipo(tipo,id,nombreArchivo, res){
   
    
    try{

        if(tipo==='usuarios'){
             let usuario= await usuerio.findByIdAndUpdate(id,{img:nombreArchivo})

             if(usuario){
                return res.status(400).json({
                    message:"Usuario no existe",
                    ok:false
                }) 
             }
              //  let pathViejo=`${__dirname}\\uploads\\${tipo}\\${usuario.img}`
       
                
                if(fs.existsSync(pathViejo)){
                    fs.unlink(pathViejo)
                }     
                    return  res.status(200).json({
                         message:"Imagen actualizada",
                         ok:true,
                         usuario
                     })
        }
        if(tipo==='medicos'){
            
            let medic= await medico.findByIdAndUpdate(id,{img:nombreArchivo})

            if(medic){
                return res.status(400).json({
                    message:"Medico no existe",
                    ok:false
                }) 
             }
            //  let pathViejo=`${__dirname}\\uploads\\${tipo}\\${usuario.img}`
                  return  res.status(200).json({
                       message:"Imagen actualizada",
                       ok:true,
                       medic
                   })
        }
        if(tipo==='hospitales'){
            let hospitales= await hospital.findByIdAndUpdate(id,{img:nombreArchivo})
            if(!hospitales){
                return res.status(400).json({
                    message:"Hospital no existe",
                    ok:false
                }) 
             }
            
            //  let pathViejo=`${__dirname}\\uploads\\${tipo}\\${usuario.img}`
     
                  return  res.status(200).json({
                       message:"Imagen actualizada",
                       ok:true,
                       usuario: hospitales
                   })
        }
    }catch(err){
        return res.status(400).json({
            message:err,
            ok:false
        }) 
    }

}
module.exports=app