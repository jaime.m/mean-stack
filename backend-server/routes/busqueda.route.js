const express = require('express')
const app = express()
const hospital = require('../models/hospital.model')
const medicos = require('../models/doctors.model')
const usuarios = require('../models/user.model')

function Buscar(regex,coleccion){

    return new Promise(async (resolve,reject)=>{
        try{
            let info= await  coleccion.find({
                nombre:regex
            })
            resolve(info)
        }catch(err){
            reject(err)
        }
    })
}
app.get('/todo/:texto',async (req,res)=>{


    try{
        let busqueda = req.params.texto
        var regex = new RegExp(busqueda,'i')

        Promise.all([Buscar(regex,hospital),
        Buscar(regex,medicos),
        Buscar(regex,usuarios)]).then(
            respuestas=>{
                res.status(200).json({
                    hospitales:respuestas[0],
                    medicos:respuestas[1],
                    usuarios:respuestas[2],
                    ok:true
                })
            }
        )
    
    }catch(err){
        res.status(200).json({
            error:err.message,
            ok:false
        })
    }

})
app.get('/coleccion/:tabla/:texto',async (req,res)=>{
    try{
        let busqueda = req.params.texto
        let coleccion = req.params.tabla
        let tabla = req.params.tabla
        var regex = new RegExp(busqueda,'i')
        
 switch(coleccion)
 {
     case('hospital'):
         coleccion=hospital
     break
     case('medicos'):
         coleccion=medicos
     break
     case('usuarios'):
         coleccion=usuarios
     break
     default:
        res.status(200).json({
            error:"Esa colección es inexistente",
            ok:false
        })

 }
        Buscar(regex,coleccion).then(
            respuestas=>{
                res.status(200).json({
                    [tabla]:respuestas,
                    ok:true
                })
            }
        )
    }catch(err){
        res.status(200).json({
            error:err.message,
            ok:false
        })
    }

})


module.exports=app