const jsonwebtoken = require('jsonwebtoken')
const {SEED} = require('../config/config')

/**
 * VERIFICAR TOKEN
 */
exports.verificaToken= function(req,res,next){
    var token = req.query.token
    jsonwebtoken.verify(token,SEED, (err,decoded)=>{
        if(err){
            return res.status(401).json({
               mensaje:"token incorrecto",
               ok:false,
               errors:err
           })
        }
        next()
    })
}

